﻿using Microsoft.Graph;
using System;
using System.IO;
using System.Net.Http.Headers;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelDataReader;
using RestSharp;
using Microsoft.Identity.Client;
using System.Security;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Net.Http;

namespace GraphConnect
{
    public class Program
    {
        private static readonly HttpClient httpClient = new HttpClient();
        static async System.Threading.Tasks.Task Main(string[] args)
        {

            //http request
            var response = await httpClient.GetAsync("https://login.microsoftonline.com/common/oauth2/v2.0/authorize?" +
                "client_id=8cbb6468-71ee-448f-ac5c-2724c4b41766" +
                "&response_type=code" +
                "&redirect_uri=https%3A%2F%2Flogin.microsoftonline.com%2Fcommon%2Foauth2%2Fnativeclient" +
                "&response_mode=query" +
                "&scope=user.read" +
                "&state=12345");
            Console.WriteLine(response);

            var authentication = new DelegateAuthenticationProvider(async request =>
            {
                request.Headers.Authorization =
                new AuthenticationHeaderValue("bearer",
                "eyJ0eXAiOiJKV1QiLCJub25jZSI6Imdmandmd2NkOWdiYUhQalFMbWNHY2FVeS1VaUhEQzRRYWNQdmRzUGdYaHMiLCJhbGciOiJSUzI1NiIsIng1dCI6IlNzWnNCTmhaY0YzUTlTNHRycFFCVEJ5TlJSSSIsImtpZCI6IlNzWnNCTmhaY0YzUTlTNHRycFFCVEJ5TlJSSSJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8yZWE4NDM1OS02YzMxLTQzOTEtYmEwMC01ZTE3ZjM5Y2QwNDEvIiwiaWF0IjoxNTk0MDkwODcxLCJuYmYiOjE1OTQwOTA4NzEsImV4cCI6MTU5NDA5NDc3MSwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IkUyQmdZRmp6d2V5V1hPdmplcW1kZ2M0eXU2YkczbzFoL2piTllrL0xyZ01lWnprMC9Dc0EiLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6IkdyYXBoIGV4cGxvcmVyIChvZmZpY2lhbCBzaXRlKSIsImFwcGlkIjoiZGU4YmM4YjUtZDlmOS00OGIxLWE4YWQtYjc0OGRhNzI1MDY0IiwiYXBwaWRhY3IiOiIwIiwiZmFtaWx5X25hbWUiOiJUYW4iLCJnaXZlbl9uYW1lIjoiSm9zaHVhIiwiaXBhZGRyIjoiMjIwLjI1NS4xNDcuMjQ1IiwibmFtZSI6Ikpvc2h1YSBUYW4iLCJvaWQiOiJhOWQzMDYwZC01YjY4LTRhZGQtYjFmYi1lZTYzNzJmMDA2MDIiLCJwbGF0ZiI6IjMiLCJwdWlkIjoiMTAwMzIwMDBCRjYxQ0Q1OCIsInNjcCI6IkZpbGVzLlJlYWQuQWxsIG9wZW5pZCBwcm9maWxlIFVzZXIuUmVhZCBlbWFpbCIsInNpZ25pbl9zdGF0ZSI6WyJrbXNpIl0sInN1YiI6Im5fVVdlZGdtbjlDbEJreEkyemFLTVAzdXp2NHFFWVViY1NBaHJ4Nm5fanciLCJ0ZW5hbnRfcmVnaW9uX3Njb3BlIjoiQVMiLCJ0aWQiOiIyZWE4NDM1OS02YzMxLTQzOTEtYmEwMC01ZTE3ZjM5Y2QwNDEiLCJ1bmlxdWVfbmFtZSI6Impvc2h1YS50YW5AdGhlbWVpbnRsdHJhZGluZy5vbm1pY3Jvc29mdC5jb20iLCJ1cG4iOiJqb3NodWEudGFuQHRoZW1laW50bHRyYWRpbmcub25taWNyb3NvZnQuY29tIiwidXRpIjoieFBCZEo5Mjd6RTJ0SWhMS1VYRTNBUSIsInZlciI6IjEuMCIsInhtc19zdCI6eyJzdWIiOiJseldHV1JfWWg2eTlEOFVFRnZnak9vOWM4TG1JSzFZYW5wUmx6MFNEQ1ZvIn0sInhtc190Y2R0IjoxNTYwNzM4MDA3fQ.Xl8fNI-zwfCohRpntdvrhj9MJDEE12HUcWCgkjeyrMKTJRxLHk9dILQyEXU0avHgSnw1vhx5cC9XzLov-FUMBmoU9V3bvCLFN_VnRaZWnTBFFCel7lUWwAVfvUtjEYWgKn_D-c-4J5CrLT_b13TuIg1ea4A5a9oacui5TkZybeLEw9CDHQQg22zNFMil9sSbyJFJrEIm-hQ9ONPJFxvDIG2YN-2XZIJkliyFXx2xNQuquH_FCanJcNmnjXFoExCB2NduChLTQGsMBib8gbD2-9BGI7iV8pfaWQBvEJo1n45yHvQlFPMIphNs4DcIVUuZCghJrrOLMHj07kp7HT-xbg");
            });
            var client = new GraphServiceClient(authentication);

            /*
            var excelFile = client.Me.Drive.Items["01L4FV2CEGIR57PUR3SZBJ5QGLWE4NOJRH"].Request().GetAsync().Result;
            var excelFileContents = client.Me.Drive.Items["01L4FV2CEGIR57PUR3SZBJ5QGLWE4NOJRH"].Content.Request().GetAsync().Result;
            
            // checking if contents are empty
            // Console.WriteLine(excelFileContents == null);


            // var reader = ExcelReaderFactory.CreateReader(excelFileContents);
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            var stream = new StreamReader(excelFileContents);
            var reader = ExcelReaderFactory.CreateOpenXmlReader(excelFileContents);

            reader.Read();
            var content = reader.GetString(0);
            reader.Read();
            var nextResult = reader.GetString(0);

            Console.WriteLine(content);
            Console.WriteLine(nextResult);

            */
            // get all files in onedrive
            
            var items = client.Me.Drive.Root.Children.Request().GetAsync().Result;
           

            Console.WriteLine(items.Count);

            Console.Read();
        }
    }
}
